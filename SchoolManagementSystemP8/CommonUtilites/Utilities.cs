﻿using SchoolManagementSystemP8.ADONET;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SchoolManagementSystemP8.CommonUtilites
{
    public static class Utilities
    {
        public static List<SelectListItem> GetProinceList()
        {
            //var db= new SMSEntities();
            using (SMSEntities db = new SMSEntities())
            {
                List<SelectListItem> list = new List<SelectListItem>();
                var data = db.TBL_Province.ToList();
                foreach (var item in data)
                {
                    //list.Add(new SelectListItem { Text = "Province One", Value = "1" });
                    //list.Add(new SelectListItem { Text = "Province Two", Value = "2" });
                    //list.Add(new SelectListItem { Text = "Province Three", Value = "3" });

                    list.Add(new SelectListItem { Text = item.ProvinceName, Value = item.ProvinceId.ToString() });
                }
                return list;
            }

        }
        public static IEnumerable<SelectListItem> GetDistrictName()
        {
            //var db= new SMSEntities();
            using (SMSEntities db = new SMSEntities())
            {
                var data1 = new SelectList(db.TBL_District.ToList(), "DistrictId", "DistrictName");
                return data1;
            }
        }
        public static IEnumerable<SelectListItem> GetRelationshipList()
        {
            //var db= new SMSEntities();
            using (SMSEntities db = new SMSEntities())
            {
                var data1 = new SelectList(db.TBL_Relationship.ToList(), "RelationshipId", "RelationshipName");
                return data1;
            }
        }
        public static IEnumerable<SelectListItem> GetStudentLevel()
        {
            //var db= new SMSEntities();
            using (SMSEntities db = new SMSEntities())
            {
                var data1 = new SelectList(db.StudentLevels.ToList(), "Id", "LevelInfo");
                return data1;
            }
        }
        public static IEnumerable<SelectListItem> GetMuncipalityList()
        {
            //var db= new SMSEntities();
            using (SMSEntities db = new SMSEntities())
            {
                var data1 = new SelectList(db.TBL_Municipality.ToList(), "MunicipalityId", "MunicipalityName");
                return data1;
            }
        }
        
    }
}