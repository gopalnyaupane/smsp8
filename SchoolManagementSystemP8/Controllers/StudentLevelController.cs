﻿using SchoolManagementSystemP8.ADONET;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SchoolManagementSystemP8.Controllers
{
    public class StudentLevelController : Controller
    {
        SMSEntities db = new SMSEntities();
        // GET: StudentLevel
        //To List the data of Student level
        public ActionResult Index()
        {
            var data = db.StudentLevels.ToList();
            return View(data);
        }
        //Generates the Create View for Student Level Creation
        public ActionResult Create()
        {
            return View();
        }
        [HttpPost]
        public ActionResult Create(StudentLevel model)
        {
            db.StudentLevels.Add(model);
            db.SaveChanges();
            return View();
        }
        public ActionResult Edit(int id)
        {
            var data = db.StudentLevels.Find(id);
            return View(data);
        }

        [HttpPost]
        public ActionResult Edit(StudentLevel obj)
        {
            var data = db.StudentLevels.Find(obj.Id);
            data.LevelInfo = obj.LevelInfo;
            db.SaveChanges();
            return RedirectToAction("Index");
        }
        public ActionResult Delete(int id)
        {
            var data = db.StudentLevels.Find(id);
            db.StudentLevels.Remove(data);
            db.SaveChanges();
            return RedirectToAction("Index");
        }
    }
}