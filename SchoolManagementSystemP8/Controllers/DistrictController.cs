﻿using SchoolManagementSystemP8.ADONET;
using SchoolManagementSystemP8.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SchoolManagementSystemP8.Controllers
{
    public class DistrictController : Controller
    {
        SMSEntities db = new SMSEntities();
        // GET: List District
        public ActionResult Index()
        {
            List<DistrictVM> list = new List<DistrictVM>();
            var data = db.TBL_District.ToList();
            foreach (var item in data)
            {
                DistrictVM model = new DistrictVM();
                model.ProvinceId = item.ProvinceId;
                model.ProvinceName = item.TBL_Province.ProvinceName;
                model.DistrictId = item.DistrictId;
                model.DistrictName = item.DistrictName;
                model.Status = item.Status??false;
                list.Add(model);
            }
            return View(list);
        }
        //Create District 
        public ActionResult Create()
        {
            return View();
        }
        //Post data
        [HttpPost]
        public ActionResult Create(DistrictVM obj)
        {
            TBL_District model = new TBL_District();
            model.DistrictId = obj.DistrictId;
            model.DistrictName = obj.DistrictName;
            model.ProvinceId = obj.ProvinceId;
            model.Status = obj.Status;
            //Add to entities
            db.TBL_District.Add(model);
            //publish changes
            db.SaveChanges();
            return RedirectToAction("Index");
        }
        //Edit district
        public ActionResult Edit(int Id)
        {
            TBL_District model = db.TBL_District.Find(Id);
            DistrictVM obj = new DistrictVM();
            obj.DistrictId = model.DistrictId;
            obj.DistrictName = model.DistrictName;
            obj.ProvinceId = model.ProvinceId;
            obj.Status = model.Status??false;
            return View(obj);
        }
        //Post data
        [HttpPost]
        public ActionResult Edit(DistrictVM obj)
        {
            TBL_District model = db.TBL_District.Find(obj.DistrictId);
            model.DistrictName = obj.DistrictName;
            model.ProvinceId = obj.ProvinceId;
            model.Status = obj.Status;
            //Add to entities
           // db.TBL_District.Add(model);
            //publish changes
            db.SaveChanges();
            return RedirectToAction("Index");
        }
        public ActionResult Delete(int Id)
        {
            TBL_District obj = db.TBL_District.Find(Id);
            db.TBL_District.Remove(obj);
            db.SaveChanges();
            return RedirectToAction("Index");
        }
    }
}