﻿using SchoolManagementSystemP8.ADONET;
using SchoolManagementSystemP8.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SchoolManagementSystemP8.Controllers
{
    public class StudentInfoController : Controller
    {
        SMSEntities db = new SMSEntities();
        // GET: StudentInfo
        public ActionResult Index()
        {
            return View();
        }
        [HttpGet]
        public ActionResult Create()
        {
            List<ParentDetailsVM> list = new List<ParentDetailsVM>();
            StudentInfoVM model = new StudentInfoVM();
            List<TBL_Relationship> rel = new List<TBL_Relationship>();
            rel = db.TBL_Relationship.ToList();
            model.RelationshipList = rel;
            var parentList = db.TBL_Relationship.ToList();
            foreach (var item in parentList)
            {
                ParentDetailsVM parent = new ParentDetailsVM();
                parent.Relationship = item.RelationshipId;
                list.Add(parent);
            }
            model.ParentList = list;

            List<TBL_EducationInfo> edulist = new List<TBL_EducationInfo>();
            var educationlist = db.StudentLevels.ToList();
            foreach (var item in educationlist)
            {
                TBL_EducationInfo model1 = new TBL_EducationInfo();
                model1.Id = item.Id;
                edulist.Add(model1);
            }
            model.EducationList = edulist;
            return View(model);
        }
        [HttpPost]
        public ActionResult Create(StudentInfoVM model)
        {
            TBL_Student stu = new TBL_Student()
            {
                FirstName=model.FirstName,
                LastName=model.LastName,
                DOB=model.DOB,
                CDistrictId=model.CDistrictId,
                CMunicipalityID=model.CMunicipalityID,
                CProvinceId=model.CProvinceId,
                CreatedDate=DateTime.Now,
                Email=model.Email,
                Gender=model.Gender,
                PDistrictID=model.PDistrictID,
                PProvinceId=model.PProvinceId,
                PWardNo=model.PWardNo,
                Phone=model.Phone,
                PMunicipalityID=model.PMunicipalityID,
            };
            db.TBL_Student.Add(stu);
            foreach (var item in model.ParentList)
            {
                TBL_ParentDetails parent = new TBL_ParentDetails()
                {
                    StudentId= stu.Id,
                    Relationship = item.Relationship,
                    ParentPhone=item.ParentPhone,
                    ParentName=item.ParentPhone,
                    Address=item.Address,
                };
                db.TBL_ParentDetails.Add(parent);
            }
            foreach (var item in model.EducationList)
            {
                TBL_EducationInfo edu = new TBL_EducationInfo()
                {
                    StudentId = stu.Id,
                    Board = item.Board,
                    College = item.College,
                    Grade = item.Grade,
                    Status = true,
                    PassedYear = item.PassedYear,
                    CreatedDate = DateTime.Now,
                };
            }
            db.SaveChanges();
            return View(model);
        }
    }
}