﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using SchoolManagementSystemP8.ADONET;
using SchoolManagementSystemP8.Models;

namespace SchoolManagementSystemP8.Controllers
{
    public class TBL_StudentController : Controller
    {
        private SMSEntities db = new SMSEntities();
        
        public ActionResult ListStudentInfo()
        {
            List<TBL_Student> listdatafromDB= db.TBL_Student.ToList();
            //Select * from TBL_Student
            return View(listdatafromDB);
        }

        public ActionResult ListProvince()
        {
            List<ProvinceVM> list = new List<ProvinceVM>();
            //creating obj1 
            ProvinceVM obj1 = new ProvinceVM();
            obj1.ProviceID = 1;
            obj1.ProvinceName = "Province One";
            list.Add(obj1);
            //creating obj1 
            ProvinceVM obj2 = new ProvinceVM();
            obj2.ProviceID = 2;
            obj2.ProvinceName = "Province Two";
            list.Add(obj2);
            //creating obj1 
            ProvinceVM obj3 = new ProvinceVM();
            obj3.ProviceID =3 ;
            obj3.ProvinceName = "Bagmati";
            list.Add(obj3);
            //creating obj1 
            ProvinceVM obj4 = new ProvinceVM();
            obj4.ProviceID = 4;
            obj4.ProvinceName = "Gandaki";
            list.Add(obj4);
            //creating obj1 
            ProvinceVM obj5 = new ProvinceVM();
            obj5.ProviceID = 5;
            obj5.ProvinceName = "Lumbini";
            list.Add(obj5);
            return View(list);
        }
        // GET: TBL_Student
        public ActionResult Index()
        {
            var data = new List<TBL_Student> {
                new TBL_Student { FirstName = "Ram", LastName = "PAndey", Email = "ram.pandey@gmail.com", Phone = "984654754" },
                new TBL_Student { FirstName = "Ram", LastName = "PAndey", Email = "ram.pandey@gmail.com", Phone = "984654754" },
                new TBL_Student { FirstName = "Ram", LastName = "PAndey", Email = "ram.pandey@gmail.com", Phone = "984654754" },
                new TBL_Student { FirstName = "Ram", LastName = "PAndey", Email = "ram.pandey@gmail.com", Phone = "984654754" }
            };
            //db.TBL_Student.ToList()
            return View(data);
        }
        public ActionResult ListInt()
        {
            //Passing data from Controller to views 
            // using model 
            //using viewbag
            //using viewdata
            List<int> list = new List<int>();
            list.Add(1);
            list.Add(2);
            list.Add(3);
            list.Add(4);
            list.Add(5);
            list.Add(6);
            //ViewData["intlist1"] = list;
            //ViewBag.intlist1 = list;
            return View(list);
        }


        // GET: TBL_Student/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TBL_Student tBL_Student = db.TBL_Student.Find(id);
            if (tBL_Student == null)
            {
                return HttpNotFound();
            }
            return View(tBL_Student);
        }

        // GET: TBL_Student/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: TBL_Student/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,FirstName,LastName,DOB,Phone,Email,Gender,PProvinceId,PDistrictID,PMunicipalityID,PWardNo,CProvinceId,CDistrictId,CMunicipalityID,Cward,CreatedBy,CreatedDate")] TBL_Student tBL_Student)
        {
            if (ModelState.IsValid)
            {
                db.TBL_Student.Add(tBL_Student);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(tBL_Student);
        }

        // GET: TBL_Student/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TBL_Student tBL_Student = db.TBL_Student.Find(id);
            if (tBL_Student == null)
            {
                return HttpNotFound();
            }
            return View(tBL_Student);
        }

        // POST: TBL_Student/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,FirstName,LastName,DOB,Phone,Email,Gender,PProvinceId,PDistrictID,PMunicipalityID,PWardNo,CProvinceId,CDistrictId,CMunicipalityID,Cward,CreatedBy,CreatedDate")] TBL_Student tBL_Student)
        {
            if (ModelState.IsValid)
            {
                db.Entry(tBL_Student).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(tBL_Student);
        }

        // GET: TBL_Student/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TBL_Student tBL_Student = db.TBL_Student.Find(id);
            if (tBL_Student == null)
            {
                return HttpNotFound();
            }
            return View(tBL_Student);
        }

        // POST: TBL_Student/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            TBL_Student tBL_Student = db.TBL_Student.Find(id);
            db.TBL_Student.Remove(tBL_Student);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
