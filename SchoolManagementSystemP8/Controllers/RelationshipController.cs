﻿using SchoolManagementSystemP8.ADONET;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SchoolManagementSystemP8.Controllers
{
    public class RelationshipController : Controller
    {
        // GET: Relationship
        SMSEntities db = new SMSEntities();
        public ActionResult Index()
        {
            return View(db.TBL_Relationship.ToList());
        }
        public ActionResult Create()
        {
            return View();
        }
        [HttpPost]
        public ActionResult Create(TBL_Relationship model)
        {
            db.TBL_Relationship.Add(model);
            db.SaveChanges();
            return RedirectToAction("Index");
        }
        public ActionResult Edit(int Id)
        {
            return View(db.TBL_Relationship.Find(Id));
        }
        [HttpPost]
        public ActionResult Edit(TBL_Relationship model)
        {
            var data = db.TBL_Relationship.Find(model.RelationshipId);
            data.RelationshipName = model.RelationshipName;
            db.SaveChanges();
            return RedirectToAction("Index");
        }
        public ActionResult Delete(int id)
        {
            var data = db.TBL_Relationship.Find(id);
            db.TBL_Relationship.Remove(data);
            db.SaveChanges();
            return RedirectToAction("Index");
        }
    }
}