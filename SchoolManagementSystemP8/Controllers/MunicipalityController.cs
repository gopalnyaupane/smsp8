﻿using SchoolManagementSystemP8.ADONET;
using SchoolManagementSystemP8.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SchoolManagementSystemP8.Controllers
{
    public class MunicipalityController : Controller
    {
        SMSEntities db = new SMSEntities();
        // GET: Municipality

        public ActionResult Index()
        {
            List<MunicipalityVM> list = new List<MunicipalityVM>();
            var data = db.TBL_Municipality.ToList();
            foreach (var item in data)
            {
                MunicipalityVM obj = new MunicipalityVM();
                obj.DistrictId = item.DistrictId;
                obj.DistrictName = item.TBL_District.DistrictName;
                obj.MunicipalityId = item.MunicipalityId;
                obj.MunicipalityName = item.MunicipalityName;
                obj.ProvinceId = item.ProvinceId;
                obj.ProvinceName = item.TBL_Province.ProvinceName;
                obj.Status = item.Status??false;
                list.Add(obj);
            }

            return View(list);
        }
        public ActionResult Create()
        {
            return View();
        }
        [HttpPost]
        public ActionResult Create(MunicipalityVM model)
        {
            TBL_Municipality obj = new TBL_Municipality()
            {
                DistrictId = model.DistrictId,
                ProvinceId=model.ProvinceId,
                MunicipalityName=model.MunicipalityName
            };
            db.TBL_Municipality.Add(obj);
            db.SaveChanges();
            return RedirectToAction("Index");
        }
        public ActionResult Edit(int Id)
        {
            var data=db.TBL_Municipality.Find(Id);
            MunicipalityVM model = new MunicipalityVM()
            {
                DistrictId = data.DistrictId,
                ProvinceId = data.ProvinceId,
                MunicipalityId = data.MunicipalityId,
                MunicipalityName = data.MunicipalityName,
                Status = data.Status ?? false,
            };
            return View(model);
        }
        [HttpPost]
        public ActionResult Edit(MunicipalityVM obj)
        {
            var data = db.TBL_Municipality.Find(obj.MunicipalityId);
            data.MunicipalityName = obj.MunicipalityName;
            data.ProvinceId = obj.ProvinceId;
            data.DistrictId = obj.DistrictId;
            data.Status = obj.Status;
            db.SaveChanges();
            return RedirectToAction("Index");
        }
        public ActionResult Delete(int id)
        {
            var data = db.TBL_Municipality.Find(id);
            db.TBL_Municipality.Remove(data);
            return RedirectToAction("Index");
        }
    }
    
}