﻿using SchoolManagementSystemP8.ADONET;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SchoolManagementSystemP8.Controllers
{
    public class ProvinceController : Controller
    {
        SMSEntities db = new SMSEntities();
        // GET: Province
        //1:List all the Province
        public ActionResult Index()
        {
            List<TBL_Province> obj = db.TBL_Province.ToList();
            return View(obj);
        }

        //2: Create Province Render Form
        [HttpGet]
        public ActionResult Create()
        {
            return View();
        }
        //3: Post Province Data
        [HttpPost]
        public ActionResult Create(TBL_Province model)
        {
            db.TBL_Province.Add(model);
            var result=db.SaveChanges();
            return View();
        }
        //Edit Method
        public ActionResult Edit(int Id=1)
        {
            TBL_Province obj = db.TBL_Province.Find(Id);
            return View(obj);
        }
        //Edit POSt
        [HttpPost]
        public ActionResult Edit(TBL_Province model)
        {
            TBL_Province obj = db.TBL_Province.Find(model.ProvinceId);
            obj.ProvinceName = model.ProvinceName;
            obj.Status = model.Status;
            var result = db.SaveChanges();
            return View();
        }
        public ActionResult Delete(int Id)
        {
            try
            {
                TBL_Province obj = db.TBL_Province.Find(Id);
                if (obj != null)
                {
                    db.TBL_Province.Remove(obj);
                }
                db.SaveChanges();
            }
            catch
            {

            }
            return RedirectToAction("Index");

        }
    }
}