﻿using SchoolManagementSystemP8.ADONET;
using SchoolManagementSystemP8.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SchoolManagementSystemP8.Controllers
{
    public class CourseController : Controller
    {
        SMSEntities db = new SMSEntities();
        // GET: Course
        public ActionResult Index()
        {
            List<CourseVM> list = new List<CourseVM>();
            var data = db.CourseDetails.ToList();
            foreach (var item in data)
            {
                CourseVM obj = new CourseVM()
                {
                    CourseId=item.CourseId,
                    CourseName=item.CourseName,
                    CreatedBy=item.CreatedBy,
                    CreatedDate=item.CreatedDate,
                    CreditHours=item.CreditHours,
                    Faculty=item.Faculty,
                    Status=item.Status
                };
                list.Add(obj);
            }
            return View(list);
        }
        public ActionResult Create()
        {
            return View();
        }
        [HttpPost] 
        public ActionResult Create(CourseVM model)
        {
            CourseDetail data = new CourseDetail()
            {
                 CourseId=model.CourseId,
                 CourseName=model.CourseName,
                 //CreatedBy=User.Identity.
                 CreatedDate=DateTime.Now,
                 CreditHours=model.CreditHours,
                 Faculty=model.Faculty,
                 Status=model.Status,
            };
            db.CourseDetails.Add(data);
            db.SaveChanges();
            return RedirectToAction("Index");
        }
        public ActionResult Edit(int id)
        {
            var model=db.CourseDetails.Find(id);
            CourseVM data = new CourseVM()
            {
                CourseId = model.CourseId,
                CourseName = model.CourseName,
                //CreatedBy=User.Identity.
                CreatedDate = DateTime.Now,
                CreditHours = model.CreditHours,
                Faculty = model.Faculty,
                Status = model.Status,
            };
            return View(data);
        }
        [HttpPost]
        public ActionResult Edit(CourseVM obj)
        {
            var olddata = db.CourseDetails.Find(obj.CourseId);
            olddata.CourseName = obj.CourseName;
            olddata.CreditHours = obj.CreditHours;
            olddata.Status = obj.Status;
            db.SaveChanges();
            return RedirectToAction("Index");
        }
        public ActionResult Delete(int id)
        {
            var data = db.CourseDetails.Find(id);
            db.CourseDetails.Remove(data);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

    }
}