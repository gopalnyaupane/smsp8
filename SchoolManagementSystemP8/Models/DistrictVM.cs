﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SchoolManagementSystemP8.Models
{
    public class DistrictVM
    {
        public int ProvinceId { get; set; }
        public int DistrictId { get; set; }
        public string DistrictName { get; set; }
        public string ProvinceName { get; set; }
        public bool Status { get; set; }
    }
}