﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SchoolManagementSystemP8.Models
{
    public class CourseVM
    {
        public int CourseId { get; set; }
        public string CourseName { get; set; }
        public Nullable<int> CreditHours { get; set; }
        public Nullable<int> Faculty { get; set; }
        public Nullable<bool> Status { get; set; }
        public string CreatedBy { get; set; }
        public Nullable<System.DateTime> CreatedDate { get; set; }
    }
}