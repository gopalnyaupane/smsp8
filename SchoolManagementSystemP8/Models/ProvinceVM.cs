﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SchoolManagementSystemP8.Models
{
    public class ProvinceVM
    {
        public int ProviceID { get; set; }
        public string ProvinceName { get; set; }
    }
}