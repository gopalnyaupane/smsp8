﻿using SchoolManagementSystemP8.ADONET;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SchoolManagementSystemP8.Models
{
    public class StudentInfoVM
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public Nullable<System.DateTime> DOB { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
        public Nullable<bool> Gender { get; set; }
        public Nullable<int> PProvinceId { get; set; }
        public Nullable<int> PDistrictID { get; set; }
        public Nullable<int> PMunicipalityID { get; set; }
        public Nullable<int> PWardNo { get; set; }
        public Nullable<int> CProvinceId { get; set; }
        public Nullable<int> CDistrictId { get; set; }
        public Nullable<int> CMunicipalityID { get; set; }
        public Nullable<int> Cward { get; set; }
        public string CreatedBy { get; set; }
        public Nullable<System.DateTime> CreatedDate { get; set; }
        public List<ParentDetailsVM> ParentList { get; set; }
        public List<TBL_EducationInfo> EducationList { get; set; }
        public List<TBL_Relationship> RelationshipList { get; set; }
    }
    public class ParentDetailsVM
    {
        public int ParentId { get; set; }
        public string ParentName { get; set; }
        public string ParentPhone { get; set; }
        public Nullable<int> Relationship { get; set; }
        public string RelationshipName { get; set; }
        public string Address { get; set; }
        public Nullable<int> StudentId { get; set; }
    }
    public class EducationInfoVM
    {
        public int Id { get; set; }
        public string Level { get; set; }
        public string College { get; set; }
        public string Board { get; set; }
        public string PassedYear { get; set; }
        public string Grade { get; set; }
        public string CreatedBy { get; set; }
        public Nullable<System.DateTime> CreatedDate { get; set; }
        public string UpdateBy { get; set; }
        public Nullable<System.DateTime> UpdatedDate { get; set; }
        public Nullable<bool> Status { get; set; }
        public int StudentId { get; set; }
    }
}